package com.design.pattern.abstract_factory;

public class X1Car implements IBMWCar {
    @Override
    public void run() {
        System.out.println("X1Car run");
    }
}
