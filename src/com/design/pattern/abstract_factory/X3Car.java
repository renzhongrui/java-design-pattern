package com.design.pattern.abstract_factory;

public class X3Car implements IBMWCar {
    @Override
    public void run() {
        System.out.println("X3Car run");
    }
}
