package com.design.pattern.abstract_factory;

public interface IBMWFactory {
    public IBMWCar createCar();

    public ILogo createLogo();
}
