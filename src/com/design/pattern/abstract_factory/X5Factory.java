package com.design.pattern.abstract_factory;

public class X5Factory implements IBMWFactory {

    @Override
    public IBMWCar createCar() {
        return new X5Car();
    }

    @Override
    public ILogo createLogo() {
        return new X5Logo();
    }
}
