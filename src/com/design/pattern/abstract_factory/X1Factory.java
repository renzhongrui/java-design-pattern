package com.design.pattern.abstract_factory;

public class X1Factory implements IBMWFactory {

    @Override
    public IBMWCar createCar() {
        return new X1Car();
    }

    @Override
    public ILogo createLogo() {
        return new X1Logo();
    }
}
