package com.design.pattern.abstract_factory;

public class X5Car implements IBMWCar {
    @Override
    public void run() {
        System.out.println("X5Car run");
    }
}
