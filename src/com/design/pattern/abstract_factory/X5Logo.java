package com.design.pattern.abstract_factory;

public class X5Logo implements ILogo {

    @Override
    public void create() {
        System.out.println("创建X5Logo");
    }
}
