package com.design.pattern.abstract_factory;

/**
 * 宝马品牌车
 */
public interface IBMWCar {
    void run();
}
