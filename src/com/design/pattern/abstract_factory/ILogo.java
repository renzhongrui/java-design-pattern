package com.design.pattern.abstract_factory;


public interface ILogo {

    void create();
}
