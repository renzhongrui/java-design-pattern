package com.design.pattern.abstract_factory;

public class X3Factory implements IBMWFactory {

    @Override
    public IBMWCar createCar() {
        return new X3Car();
    }

    @Override
    public ILogo createLogo() {
        return new X3Logo();
    }
}
