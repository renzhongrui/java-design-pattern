package com.design.pattern.abstract_factory;

public class Test {

    public static void main(String[] args) {
        // 创建X1工厂类，并创建X1车和X1车标
        X1Factory x1Factory = new X1Factory();
        IBMWCar carX1 = x1Factory.createCar();
        carX1.run();
        ILogo logo1 = x1Factory.createLogo();
        logo1.create();

        // 创建X3工厂类，并创建X3车和X3车标
        X3Factory x3Factory = new X3Factory();
        IBMWCar carX3 = x3Factory.createCar();
        carX3.run();
        ILogo logo3 = x3Factory.createLogo();
        logo3.create();

        // 创建X5工厂类，并创建X5车和X5车标
        X5Factory x5Factory = new X5Factory();
        IBMWCar carX5 = x5Factory.createCar();
        carX5.run();
        ILogo logo5 = x5Factory.createLogo();
        logo5.create();
    }
}
