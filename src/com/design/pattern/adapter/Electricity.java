package com.design.pattern.adapter;

/**
 * 电能
 */
public class Electricity {

    public int output() {
        System.out.println("输出电压：220");
        return 220;
    }

}
