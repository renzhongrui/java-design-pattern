package com.design.pattern.adapter;

public class Test {

    public static void main(String[] args) {
        Battery battery = new Battery();
        ChargerAdapter adapter = new ChargerAdapter();
        battery.setAdapter(adapter);
        int quantity = battery.getQuantity();
        System.out.println("接收电压大小：" + quantity);
    }
}
