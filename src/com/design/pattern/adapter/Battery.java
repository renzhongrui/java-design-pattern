package com.design.pattern.adapter;

/**
 * 手机使用的电池
 */
public class Battery {
    private int quantity;

    public void setAdapter(ChargerAdapter adapter) {
        this.quantity = adapter.output5V();
    }

    public int getQuantity() {
        return quantity;
    }

}
