package com.design.pattern.adapter;

public class ChargerAdapter implements IAdapter {
    Electricity electricity = new Electricity();

    @Override
    public int output5V() {
        int v220 = electricity.output();
        int v5 = v220 / 44;
        return v5;
    }
}
