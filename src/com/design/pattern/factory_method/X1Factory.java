package com.design.pattern.factory_method;

public class X1Factory implements IBMWFactory {

    @Override
    public IBMWCar createCar() {
        return new X1Car();
    }
}
