package com.design.pattern.factory_method;

public class X1Car implements IBMWCar {
    @Override
    public void run() {
        System.out.println("X1Car run");
    }
}
