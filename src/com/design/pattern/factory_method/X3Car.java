package com.design.pattern.factory_method;

public class X3Car implements IBMWCar {
    @Override
    public void run() {
        System.out.println("X3Car run");
    }
}
