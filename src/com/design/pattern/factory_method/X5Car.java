package com.design.pattern.factory_method;

public class X5Car implements IBMWCar {
    @Override
    public void run() {
        System.out.println("X5Car run");
    }
}
