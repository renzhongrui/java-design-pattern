package com.design.pattern.factory_method;

public class Test {

    public static void main(String[] args) {
        // 创建X1工厂类，并创建X1车
        X1Factory x1Factory = new X1Factory();
        IBMWCar carX1 = x1Factory.createCar();
        carX1.run();

        // 创建X3工厂类，并创建X1车
        X3Factory x3Factory = new X3Factory();
        IBMWCar carX3 = x3Factory.createCar();
        carX3.run();

        // 创建X5工厂类，并创建X1车
        X5Factory x5Factory = new X5Factory();
        IBMWCar carX5 = x5Factory.createCar();
        carX5.run();
    }
}
