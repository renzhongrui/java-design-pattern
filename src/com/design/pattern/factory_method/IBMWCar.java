package com.design.pattern.factory_method;

/**
 * 宝马品牌车
 */
public interface IBMWCar {
    void run();
}
