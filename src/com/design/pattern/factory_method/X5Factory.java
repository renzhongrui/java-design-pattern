package com.design.pattern.factory_method;

public class X5Factory implements IBMWFactory {

    @Override
    public IBMWCar createCar() {
        return new X5Car();
    }
}
