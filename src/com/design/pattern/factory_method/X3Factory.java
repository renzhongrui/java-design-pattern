package com.design.pattern.factory_method;

public class X3Factory implements IBMWFactory {

    @Override
    public IBMWCar createCar() {
        return new X3Car();
    }
}
