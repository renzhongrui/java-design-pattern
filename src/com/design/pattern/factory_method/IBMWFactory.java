package com.design.pattern.factory_method;

public interface IBMWFactory {
    public IBMWCar createCar();
}
