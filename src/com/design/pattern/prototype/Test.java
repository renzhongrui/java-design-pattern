package com.design.pattern.prototype;

public class Test {

    public static void main(String[] args) throws CloneNotSupportedException {
        ACar car = new Car();
        for (int i = 0; i < 10; i++) {
            Car clone = (Car) car.clone();
            clone.setName(i);
            clone.setEngine("volvo");
            clone.setCpu("麒麟");
            clone.setWheel("四轮");
            System.out.println(clone.getName() + ":" + clone);
        }
    }
}
