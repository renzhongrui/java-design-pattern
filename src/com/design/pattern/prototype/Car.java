package com.design.pattern.prototype;

import javax.xml.crypto.Data;
import java.util.Date;

public class Car extends ACar {

    private int name;
    private String engine;
    private String wheel;
    private String cpu;
    private Date date;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Car clone = (Car) super.clone();
        // 深克隆
        clone.date = (Date) clone.date.clone();
        return clone;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getWheel() {
        return wheel;
    }

    public void setWheel(String wheel) {
        this.wheel = wheel;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

}
